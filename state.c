/*
** state.c for  in /home/ladouc_f/rendu/PSU_2013_philo
** 
** Made by ladouc_f
** Login   <ladouc_f@epitech.net>
** 
** Started on  Sun Mar 23 20:26:33 2014 ladouc_f
** Last update Sun Mar 23 20:37:14 2014 ladouc_f
*/

#include "philosophe.h"
extern pthread_mutex_t		g_philo[PHILOSOPHER];
extern t_philo			g_tab[PHILOSOPHER];
extern int			g_riceBowlcount;

void		stick_right(int id)
{
  if (g_tab[(id + 1) % PHILOSOPHER].stick == 1)
    {
      pthread_mutex_lock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[(id + 1) % PHILOSOPHER].stick = 0;
      pthread_mutex_unlock(&g_philo[(id + 1) % PHILOSOPHER]);
      printf("philosopher %d eats his rice bowl\n\n", id);
      g_tab[id].nbRiceBowl++;
      g_riceBowlcount++;
      pthread_mutex_lock(&g_philo[id]);
      pthread_mutex_lock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[id].stick = 1;
      g_tab[(id + 1) % PHILOSOPHER].stick = 1;
      pthread_mutex_unlock(&g_philo[id]);
      pthread_mutex_unlock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[id].state = 1;
    }
  else
    {
      pthread_mutex_lock(&g_philo[id]);
      g_tab[id].stick = 1;
      pthread_mutex_unlock(&g_philo[id]);
    }
}

void		eating(int id)
{
  if (g_tab[id].stick == 1)
    {
      pthread_mutex_lock(&g_philo[id]);
      g_tab[id].stick = 0;
      pthread_mutex_unlock(&g_philo[id]);
      stick_right(id);
    }
}

void		sleeping(int id)
{
  printf("Philosopher %d makes a BIG sleep.\n\n", id);
  pthread_mutex_lock(&g_philo[id]);
  g_tab[id].stick = 1;
  pthread_mutex_unlock(&g_philo[id]);
  g_tab[id].state = 2;
}

void		thinking(int id)
{
  if (g_tab[id].stick == 1)
    {
      pthread_mutex_lock(&g_philo[id]);
      g_tab[id].stick = 0;
      pthread_mutex_unlock(&g_philo[id]);
      printf("Philosopher %d is thinking.\n\n", id);
      pthread_mutex_lock(&g_philo[id]);
      g_tab[id].stick = 1;
      pthread_mutex_unlock(&g_philo[id]);
      g_tab[id].state = 0;
    }
  else if (g_tab[(id + 1) % PHILOSOPHER].stick == 1)
    {
      pthread_mutex_lock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[(id + 1) % PHILOSOPHER].stick = 0;
      pthread_mutex_unlock(&g_philo[(id + 1) % PHILOSOPHER]);
      printf("Philosopher %d is thinking.\n\n", id);
      pthread_mutex_lock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[(id + 1) % PHILOSOPHER].stick = 1;
      pthread_mutex_unlock(&g_philo[(id + 1) % PHILOSOPHER]);
      g_tab[id].state = 0;
    }
}
