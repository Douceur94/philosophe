##
## Makefile for  in /home/ladouc_f/rendu/PSU_2013_philo
## 
## Made by ladouc_f
## Login   <ladouc_f@epitech.net>
## 
## Started on  Sun Mar 23 20:24:06 2014 ladouc_f
## Last update Sun Mar 23 20:26:10 2014 ladouc_f
##

CC	= gcc

RM	= rm -rf

CFLAGS	+= -Wextra -Wall -Werror

NAME	= philo

SRCS	= philosophe.c \
	state.c \

OBJS	= $(SRCS:.c=.o)

all:	$(NAME)

$(NAME): $(OBJS)
	@$(CC) $(OBJS) -o $(NAME) -lpthread
clean:
	@$(RM) $(OBJS)

fclean: clean
	@$(RM) $(NAME)

re:	fclean all

.PHONY:	all clean fclean re
