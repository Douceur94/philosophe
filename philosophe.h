/*
** philosophe.h for  in /home/ladouc_f/rendu/PSU_2013_philo
** 
** Made by ladouc_f
** Login   <ladouc_f@epitech.net>
** 
** Started on  Tue Mar 18 16:15:09 2014 ladouc_f
** Last update Sun Mar 23 20:47:54 2014 ladouc_f
*/

#ifndef _PHILOSOPHE_H__
# define _PHILOSOPHE_H__

# include <pthread.h>
# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <stdbool.h>
# include <unistd.h>
# define PHILOSOPHER 7
# define MAX_RICE_BOWL 101

typedef struct		s_philo
{
  int			stick;
  int			id_philo;
  int			nbRiceBowl;
  int			state;
}			t_philo;
void			stick_right(int id);
void			eating(int id);
void			sleeping(int id);
void			thinking(int id);

#endif /* !_PHILOSOPHE_H__ */
