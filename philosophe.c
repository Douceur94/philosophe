/*
** philosophe.c for  in /home/ladouc_f/rendu/PSU_2013_philo
** 
** Made by ladouc_f
** Login   <ladouc_f@epitech.net>
** 
** Started on  Fri Mar 21 18:00:22 2014 ladouc_f
** Last update Sun Mar 23 20:46:20 2014 ladouc_f
*/

#include "philosophe.h"

int			g_riceBowlcount = 0;

pthread_mutex_t		g_philo[PHILOSOPHER];

t_philo			g_tab[PHILOSOPHER] =
  {
    {1, 1, 0, 0},
    {1, 2, 0, 0},
    {1, 3, 0, 0},
    {1, 4, 0, 0},
    {1, 5, 0, 0},
    {1, 6, 0, 0},
    {1, 7, 0, 0}
  };

void		*philosopher(void *arg)
{
  int		id;

  id = *(int *)arg;
  while (g_riceBowlcount < MAX_RICE_BOWL)
    {
      if (g_tab[id].state == 0)
	eating(id);
      else if (g_tab[id].state == 1)
      	sleeping(id);
      else if (g_tab[id].state == 2)
      	thinking(id);
      sleep(3);
    }
  printf("Philosopher %d has finished the amazing dinner !\n", id);
  pthread_exit(0);
}

int		pthread_create_init(pthread_t th_philo[PHILOSOPHER])
{
  int		i;

  i = 0;
  while (i < PHILOSOPHER)
    {
      if (pthread_mutex_init(&g_philo[i], NULL) != 0)
	{
	  perror("Error, pthread_init.");
	  return (1);
	}
      i++;
    }
  i = 0;
  while (i < PHILOSOPHER)
    {
      if (pthread_create(&th_philo[i], NULL, philosopher, &g_tab[i].id_philo)
	  != 0)
	{
	  perror("Error, pthread_create.");
	  return (1);
	}
      i++;
      usleep(1500000);
    }
  return (0);
}

int		main()
{
  pthread_t	th_philo[PHILOSOPHER];
  void		*status;
  int		i;

  if (pthread_create_init(th_philo) == 1)
    return (1);
  i = 0;
  while (i < PHILOSOPHER)
    {
      if (pthread_join(th_philo[i], &status) != 0)
        {
	  perror("Error pthread_join.");
	  return (1);
        }
      i++;
    }
  printf("\n");
  i = 1;
  while (i <= PHILOSOPHER)
    {
      printf("Philosopher %d ate %d RiceBowl.\n", i, g_tab[i].nbRiceBowl);
      i++;
    }
  return (0);
}
